import { Router } from 'express';
import fetch from 'node-fetch';
import passport from 'passport';
import { SERVICES_URL } from '@hgb/config/environment';

const router = Router();

/**
 * Checking
 */
router.get('/check-status', async (req, res) => {
  return res.status(200).send({ message: 'OK' });
});

/**
 * GET
 */
router.get('/get-offer-by-id', async (req, res, next) => {
  const { query: { id } } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/read-offer?id=${id}`);
    const json = await response.json();
    if (json.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});

router.get('/get-all-user-offer', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const { user: { _id}, query: { page = 0 } } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/get-offer-by-user?userId=${_id}&page=${page}`);
    const json = await response.json();
    if (response.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});

router.get('/get-all-partner-offer', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const { user: { _id }, query: { page = 0 } } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/get-offer-by-partner?partnerId=${_id}&page=${page}`);
    const json = await response.json();
    if (response.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});


/**
 * POST
 */
router.post('/create', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const { user: { _id}, body } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/create-offer`, { 
      method: 'POST',
      body: JSON.stringify({
        partner: _id,
        ...body
      }),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    if (json.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});

export default router;