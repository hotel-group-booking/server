import { Schema, Document, model } from 'mongoose';

interface IOfferModel extends Document {
  partner: string
  user: string
  created: Date
  detail: any,
  deleted: boolean
}

const schema = new Schema({
  partner: { type: String, required: true },
  user: { type: String, required: true },
  created: { type: Date, default: Date.now() },
  detail: Object,
  deleted: { type: Boolean, default: false }
})

const offer = model<IOfferModel>('Offer', schema);
export {
  IOfferModel,
  offer as Offer
}