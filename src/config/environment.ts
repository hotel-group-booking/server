const SALT_ROUNDS = 10;
const PER_PAGE = 20;
const SECRET = 'secret';
const DATABASE_URL = process.env.DATABASE_URL || '';
const SERVICES_URL = process.env.SERVICES_URL || '';

export {
  SALT_ROUNDS,
  DATABASE_URL,
  PER_PAGE,
  SERVICES_URL,
  SECRET
}