import { curry, mean, compose, divide, __, subtract, negate, add, multiply } from 'ramda';
import { IRequestModel } from '@hgb/models';

const pointBaseFrom = (points: number[]) => compose(
  divide(__, 10),
  mean
)(points);

const calculateWith = curry((criteria: any, request: IRequestModel, property: (criteria: any, request: IRequestModel) => number) => property(criteria, request));

const budget = ({ budgetFrom, budgetTo }: { budgetFrom: number, budgetTo: number }, info: IRequestModel) => {
  const pointBase = pointBaseFrom([budgetFrom, budgetTo]);
  const fromDiff = subtract(info.budgetFrom, budgetFrom);
  const toDiff = subtract(info.budgetTo, budgetTo);

  const penalty = compose(
    multiply(pointBase),
    divide(__, mean([budgetFrom, budgetTo]))
  );

  const point = add(
    fromDiff >= 0 ? pointBase : penalty(fromDiff),
    toDiff <= 0 ? pointBase: compose(negate, penalty)(toDiff)
  )

  return point;
}

const roomType = ({ roomType, budgetFrom, budgetTo }, info: IRequestModel) => {
  const pointBase = pointBaseFrom([budgetFrom, budgetTo]) * 2;

  const diff = Math.abs(roomType - info.roomType);
  const penalty = compose(multiply(pointBase), multiply(0.5));

  return pointBase - penalty(diff);
}

const starRating = ({ starRating, budgetFrom, budgetTo }, info: IRequestModel) => {
  const pointBase = pointBaseFrom([budgetFrom, budgetTo]) * 2;

  const diff = Math.abs(starRating - info.starRating);
  const penalty = compose(multiply(pointBase), multiply(0.5));

  return pointBase - penalty(diff);
}

export {
  calculateWith,
  budget,
  roomType,
  starRating
}