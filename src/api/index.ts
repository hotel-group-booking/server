import { Router } from 'express';
import AuthRouter from './routers/auth.api';
import UserRouter from './routers/user.api';
import RequestRouter from './routers/request.api';
import OfferRouter from './routers/offer.api';

const router = Router();

router.use('/auth', AuthRouter);
router.use('/user', UserRouter);
router.use('/request', RequestRouter);
router.use('/offer', OfferRouter);

export default router;