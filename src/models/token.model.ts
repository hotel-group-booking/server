import { Schema, Document, model } from 'mongoose';

interface ITokenModel extends Document {
  value: string
}

const schema = new Schema({
  value: { type: String, required: true }
});

const token = model<ITokenModel>('Token', schema);

export {
  token as Token
}