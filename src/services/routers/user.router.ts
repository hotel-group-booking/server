import { Router } from 'express';
import { merge } from 'ramda';
import { User } from '@hgb/models';

const router = Router();

/**
 * Checking...
 */
router.get('/check-status', async (req, res) => {
  res.status(200).send({ status: 'OK' });
});

/**
 * GET 
 */
router.get('/read-by-id', async (req, res, next) => {
  const { query: { id } } = req;
  try {
    const user = await User.findById(id);
    if (!user) {
      return res.status(404).send({ message: `can not find user with id: ${id}` });
    }
    return res.status(200).send({
      user: user.toData(),
      message: 'OK'
    });
  } catch (err) {
    next(err);
  }
});

router.get('/read-by-username', async (req, res, next) => {
  const { query: { username } } = req;
  try {
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(404).send({ message: `can not find user with username: ${username}`});
    }
    return res.status(200).send({
      user: user.toData(),
      message: 'OK'
    })
  } catch (err) {
    next(err);
  }
});

router.get('/all-account', async (req, res, next) => {
  const { query: { page, included = true, role, perPage } } = req;
  const query = {};
  if (!JSON.parse(included)) {
    query['deleted'] = false;
  }
  if (role) {
    query['role'] = role;
  }
  try {
    const users = await User.find(query, null, { skip: page * perPage, limit: +perPage });
    const total = await User.countDocuments(query);
    return res.status(200).send({ 
      total,
      users: users.map(user => user.toData()), 
      count: users.length,
      message: 'OK' 
    });
  } catch (err) {
    next(err);
  }
});

router.get('/count', async (req, res, next) => {
  const { query: { included = true } } = req;
  const query = {};
  if (!JSON.parse(included)) {
    query['deleted'] = false;
  }
  try {
    const user = await User.countDocuments(merge(query, { role: 'user' }));
    const partner = await User.countDocuments(merge(query, { role: 'partner' }));
    const admin = await User.countDocuments(merge(query, { role: 'admin' }));
    const total = await User.countDocuments(query);

    return res.status(200).send({
      user,
      admin,
      partner,
      total,
      message: 'OK'
    });
  } catch (err) {
    next(err);
  }
});

router.get('/token', async (req, res, next) => {
  const { query: { id } } = req;
  try {
    const user = await User.findById(id);
    if (!user) {
      return res.status(404).send({ message: `can not find user with id ${id}`, token: null });
    }
    return res.status(200).send({
      message: 'OK',
      token: user.generateJWT()
    });
  } catch (err) {
    next(err);
  }
});

/**
 * POST
 */
router.post('/create', async (req, res, next) => {
  const { body } = req;
  try {
    const checker = await User.findOne({ username: body.username });
    if (checker) {
      return res.status(403).send({ message: 'Username has been used' });
    }
    const user = new User(body);
    const result = await user.save();
    return res.status(200).send({ message: 'OK', user: result });
  } catch (err) {
    next(err);
  }
});

router.post('/update', async (req, res, next) => {
  const { body: { id, ...rest } } = req;
  try {
    const updatedUser = await User.findByIdAndUpdate(id, rest, {
      new: true
    });
    if (!updatedUser) {
      return res.status(404).send({ message: `can not update user with id: ${id}, may be not exsisted`, user: null })
    }
    return res.status(200).send({ message: 'OK', user: updatedUser.toData() });

  } catch (err) {
    next(err);
  }
});

router.post('/delete', async (req, res, next) => {
  const { body: { id } } = req;
  try {
    const user = await User.findByIdAndUpdate(id, { deleted: true }, { new: true });
    if (!user) {
      return res.status(404).send({ message: `can not delete user with id: ${id}, may be not exsisted`, user: null })
    }
    return res.status(200).send({ user: user.toData(), message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/destroy', async (req, res, next) => {
  const { body: { id } } = req;
  try {
    const user = await User.findByIdAndDelete(id);
    if (!user) {
      return res.status(404).send({ message: `can not destroy user with id: ${id}, may be not exsisted`, user: null })
    }
    return res.status(200).send({ user: user.toData(), message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/compare-password', async (req, res, next) => {
  const { query: { role = ['user'] }, body: { username, password } } = req;
  try {
    const user = await User.findOne({ username, role: { $in: role } });
    if (!user) {
      return res.status(403).send({ message: 'invalid username' });
    }
    const match = await user.comparePassword(password);
    if (!match) {
      return res.status(403).send({ message: 'invalid password' });
    }
    return res.status(200).send({
      user: user.toAuthJSON(),
      message: 'OK'
    });
  } catch (err) {
    next(err);
  }
});

export default router;