import { Router } from 'express';
import passport from 'passport';
import fetch from 'node-fetch';
import { SERVICES_URL } from '@hgb/config/environment';

const router = Router();

/**
 * Checking
 */
router.get('/check-status', async (req, res) => {
  res.status(200).send({ message: 'OK' });
});

/**
 * GET
 */

router.get('/get-all-request', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const { user: { _id }, query: { page = 0 } } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/get-request-by-user?userId=${_id}&page=${page}`);
    const json = await response.json();
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});

router.get('/statictis', async (req, res, next) => {
  try {
    const response = await fetch(`${SERVICES_URL}/business/count`);
    const json = await response.json();
    if (response.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});

/**
 * POST
 */
router.post('/create', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const { user: { _id }, body } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/create-request`, {
      method: 'POST',
      body: JSON.stringify({
        user: _id,
        ...body
      }),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    if (json.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send({ request: json.request, message: json.message });
  } catch (err) {
    next(err);
  }
});

router.post('/filter-request', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const { body, query: { page = 0 } } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/business/filter-request?page=${page}`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    return res.status(200).send(json);
  } catch (err){
    next(err);
  }
});


export default router;