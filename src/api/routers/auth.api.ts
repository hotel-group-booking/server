import { Router } from 'express';
import passport from 'passport';
import fetch from 'node-fetch';
import { ExtractJwt } from 'passport-jwt';
import { Token } from '@hgb/models';
import { SERVICES_URL } from '@hgb/config/environment';

const router = Router();

router.get('/check-status', async (req, res) => {
  res.status(200).send({ message: 'OK' });
});

/**
 * GET
 */
router.get('/user-logout', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  const value = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
  try {
    const token = new Token({ value });
    const result = await token.save();
    if (!result) {
      return res.status(500).send({ message: 'Database Error' });
    }
    return res.status(200).send({ message: 'OK', expiredToken: token });
  } catch(err) {
    next(err);
  }
});

router.get('/admin-logout', passport.authenticate('admin-jwt', { session: false }), async (req, res, next) => {
  const value = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
  try {
    const token = new Token({ value });
    const result = await token.save();
    if (!result) {
      return res.status(500).send({ message: 'Database Error' });
    }
    return res.status(200).send({ message: 'OK', expiredToken: token });
  } catch(err) {
    next(err);
  }
});

router.get('/current-user', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  try {
    const { user: { _id } } = req;
    const response = await fetch(`${SERVICES_URL}/user/read-by-id?id=${_id}`);
    const json = await response.json();
    if (response.status === 404) {
      return res.status(403).send({ message: json.message });
    }
    return res.status(200).send({ user: json.user, message: json.message });
  } catch (err) {
    next(err);
  }
});

router.get('/current-admin', passport.authenticate('admin-jwt', { session: false }), async (req, res, next) => {
  try {
    const { user: { _id } } = req;
    const response = await fetch(`${SERVICES_URL}/user/read-by-id?id=${_id}`);
    const json = await response.json();
    if (response.status === 404) {
      return res.status(403).send({ message: json.message });
    }
    return res.status(200).send({ user: json.user, message: json.message });
  } catch (err) {
    next(err);
  }
});

router.get('/user-token-verify', passport.authenticate('user-jwt', { session: false }), async (req, res, next) => {
  try {
    const { user: { _id } } = req;
    const response = await fetch(`${SERVICES_URL}/user/token?id=${_id}`);
    const json = await response.json();
    if (json.status === 404) {
      return res.status(403).send({ token: null , message: json.message });
    }
    return res.status(200).send({ token: json.token, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.get('/admin-token-verify', passport.authenticate('admin-jwt', { session: false }), async (req, res, next) => {
  try {
    const { user: { _id } } = req;
    const response = await fetch(`${SERVICES_URL}/user/token?id=${_id}`);
    const json = await response.json();
    if (json.status === 404) {
      return res.status(403).send({ token: null , message: json.message });
    }
    return res.status(200).send({ token: json.token, message: 'OK' });
  } catch (err) {
    next(err);
  };
});

/**
 * POST
 */
router.post('/user-login', async (req, res, next) => {
  passport.authenticate('user-local', { session: false }, (err, user, info) => {
    if (err) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    if (!user) {
      return res.status(info.statusCode).send({ message: info.message });
    }

    req.login(user, { session: false }, err => {
      if (err) {
        next(err);
      }
      return res.status(200).send({user, message: info.message});
    })
  })(req, res);
});

router.post('/admin-login', async (req, res, next) => {
  passport.authenticate('admin-local', { session: false }, (err, user, info) => {
    if (err) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    if (!user) {
      return res.status(info.statusCode).send({ message: info.message });
    }

    req.login(user, { session: false }, err => {
      if (err) {
        next(err);
      }
      return res.status(200).send({user, message: info.message});
    })
  })(req, res);
});
export default router;