import { Schema, Document, model } from 'mongoose';
import { compare, hash } from 'bcrypt';
import jwt from 'jsonwebtoken';
import { SALT_ROUNDS, SECRET } from '@hgb/config/environment';

interface IUserModel extends Document {
  getDisplayName(): string
  comparePassword(candidatePassword: string): boolean
  generateJWT(): string
  toAuthJSON(): object
  toData(): object
  provider: string
  username: string
  password: string
  displayName: string
  lastName: string
  firstName: string
  email: string
  photos: string
  created: Date
  role: string
  phone: string
  hotelInformation: any
  active: boolean
  deleted: boolean
}

const schema = new Schema({
  provider: { type: String, default: 'local' },
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  displayName: String,
  lastName: { type: String, required: true },
  firstName: { type: String, required: true },
  email: String,
  photos: String,
  created: { type: Date, default: Date.now() },
  deleted: { type: Boolean, default: false },
  role: { type: String, default: 'user' },
  phone: { type: String, required: true },
  hotelInformation: Object,
  active: { type: Boolean, default: true }
});

/**
 * Instance methods
 */
schema.methods.getDisplayName = function(): string {
  return this.displayName || `${this.firstName} ${this.lastName}`;
};

schema.methods.comparePassword = async function(candidatePassword) {
  try {
    const match = await compare(candidatePassword, this.password);
    return match;
  } catch (err) {
    throw err;
  }
};

schema.methods.generateJWT = function() {
  const today = new Date();
  const exp = new Date(today);
  exp.setDate(today.getDate() + 7);

  return jwt.sign({
    id: this._id,
    role: this.role,
    active: this.active,
    deleted: this.deleted,
    exp: exp.getTime()
  }, SECRET);
};

schema.methods.toAuthJSON = function() {
  return {
    id: this._id,
    role: this.role,
    active: this.active,
    token: this.generateJWT()
  };
};

schema.methods.toData = function() {
  if (!this.displayName) {
    this.displayName = this.getDisplayName();
  }
  return this;
}

/**
 * Hooks
 */
schema.pre<IUserModel>('save', async function(next) {
  if (!this.isModified('password')) {
    return next();
  }

  try {
    const hashedPassword = await hash(this.password, SALT_ROUNDS);
    this.password = hashedPassword;
    return next();
  } catch (err) {
    return next(err);
  }
});

schema.pre('findOneAndUpdate', async function(next) {
  const user = this.getUpdate();
  if (!user.password) {
    return next();
  }

  try {
    const hashedPassword = await hash(user.password, SALT_ROUNDS);
    user.password = hashedPassword;
    return next();
  } catch (err) {
    return next(err);
  }
});

const user = model<IUserModel>('User', schema);

export {
  IUserModel,
  user as User
}

