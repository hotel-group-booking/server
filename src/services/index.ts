import { Router } from 'express';
import UserRouter from './routers/user.router';
import BusinessRouter from './routers/business.router';

const router = Router();

router.use('/user', UserRouter);
router.use('/business', BusinessRouter);

export default router;