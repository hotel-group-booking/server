import { Router } from 'express';
import fetch from 'node-fetch';
import { SERVICES_URL, PER_PAGE } from '@hgb/config/environment'; 

const router = Router();

/**
 * Checking
 */
router.get('/check-status', (req, res) => {
  res.status(200).send({ message: 'OK' });
});

/**
 * GET
 */
router.get('/statictis', async (req, res, next) => {
  try {
    const response = await fetch(`${SERVICES_URL}/user/count`);
    const json = await response.json();
    if (response.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    return res.status(200).send(json);
  } catch (err) {
    next(err);
  }
});

router.get('/fetch-partners', async (req, res, next) => {
  const { query: { page = 0, included = false, perPage = PER_PAGE }} = req;
  try {
    const response = await fetch(`${SERVICES_URL}/user/all-account?page=${page}&included=${included}&role=partner&perPage=${perPage}`);
    const json = await response.json();
    return res.status(response.status).send(json);
  } catch(err) {
    next(err);
  }
});

router.get('/fetch-detail', async (req, res, next) => {
  const { query: { id } } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/user/read-by-id?id=${id}`);
    const json = await response.json();
    return res.status(response.status).send(json);
  } catch (err) {
    next(err);
  }
});

/**
 * POST
 */
router.post('/registration', async (req, res, next) => {
  const { body } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/user/create`, {
      method: 'POST',
      body: JSON.stringify({
        active: body.role === 'partner' ? false : true,
        ...body
      }),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    if (response.status === 500) {
      return res.status(500).send({ message: 'Internal Server Error' });
    }
    if (response.status === 403) {
      return res.status(403).send({ message: json.message, user: null });
    }
    return res.status(200).send({ user: json.user, message: json.message });
  } catch (err) {
    next(err);
  }
});

router.post('/update', async (req, res, next) => {
  const { body } = req;
  try {
    const response = await fetch(`${SERVICES_URL}/user/update`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    return res.status(response.status).send(json);
  } catch (err) {
    next(err);
  }
});

export default router;