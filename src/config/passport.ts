import passport from 'passport';
import fetch from 'node-fetch';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { SERVICES_URL, SECRET } from '@hgb/config/environment';
import { Token } from '@hgb/models';
passport.use('user-local', new LocalStrategy(async (username, password, done) => {
  try {
    const response = await fetch(`${SERVICES_URL}/user/compare-password?role=user&role=partner`, {
      method: 'POST',
      body: JSON.stringify({
        username, 
        password
      }),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    if (response.status === 500) {
      return done(null, false, { message: 'Internal Server Error', error: JSON.stringify(json), statusCode: 500 } as any);
    }
    if (response.status === 403) {
      return done(null, false, { message: json.message, statusCode: 403 } as any);
    }
    return done(null, json.user , { message: json.message });
  } catch (err) {
    done(err);
  }
}));

passport.use('admin-local', new LocalStrategy(async (username, password, done) => {
  try {
    const response = await fetch(`${SERVICES_URL}/user/compare-password?role=admin`, {
      method: 'POST',
      body: JSON.stringify({
        username, 
        password
      }),
      headers: { 'Content-Type': 'application/json' }
    });
    const json = await response.json();
    if (response.status === 500) {
      return done(null, false, { message: 'Internal Server Error', error: JSON.stringify(json), statusCode: 500 } as any);
    }
    if (response.status === 403) {
      return done(null, false, { message: json.message, statusCode: 403 } as any);
    }
    return done(null, json.user , { message: json.message });
  } catch (err) {
    done(err);
  }
}));

passport.use('user-jwt', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET,
  passReqToCallback: true
}, async (req, jwtPayload, done) => {
  const { id, exp } = jwtPayload;
  try {
    if (Date.now() > exp) {
      return done(null, false, { message: 'Token expired', statusCode: 401 });
    }

    const rawToken = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
    const token = await Token.findOne({ value: rawToken });
    if (token) {
      return done(null, false, { message: 'Token expired', statusCode: 401 });
    }

    const response = await fetch(`${SERVICES_URL}/user/read-by-id?id=${id}`);

    const json = await response.json();

    if (response.status === 500) {
      return done(null, false, { message: 'Internal Server Error', error: JSON.stringify(json), statusCode: 500 });
    }
    if (response.status === 404) {
      return done(null, false, { message: json.message, statusCode: 401 });
    }
    if (json.user.role === 'admin') {
      return done(null, false, { message: 'Access Denied', statusCode: 401 });
    }
    return done(null, json.user, { message: json.message });

  } catch (err) {
    return done(err);
  }
}));

passport.use('admin-jwt', new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET,
  passReqToCallback: true
}, async (req, jwtPayload, done) => {
  const { id, exp } = jwtPayload;
  try {
    if (Date.now() > exp) {
      return done(null, false, { message: 'Token expired', statusCode: 401 });
    }

    const rawToken = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
    const token = await Token.findOne({ value: rawToken });
    if (token) {
      return done(null, false, { message: 'Token expired', statusCode: 401 });
    }

    const response = await fetch(`${SERVICES_URL}/user/read-by-id?id=${id}`);

    const json = await response.json();

    if (response.status === 500) {
      return done(null, false, { message: 'Internal Server Error', error: JSON.stringify(json), statusCode: 500 });
    }
    if (response.status === 404) {

      return done(null, false, { message: json.message, statusCode: 401 });
    }
    if (json.user.role !== 'admin') {
      return done(null, false, { message: 'Access Denied', statusCode: 401 });
    }
    return done(null, json.user, { message: json.message });

  } catch (err) {
    return done(err);
  }
}));
