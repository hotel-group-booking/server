import express, { Request, Response, NextFunction } from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import colors from 'colors/safe';
import passport from 'passport';
import mongoose from 'mongoose';
import { DATABASE_URL } from '@hgb/config/environment';
import services from '@hgb/services';
import api from '@hgb/api';

/**
 * Config
 */
import('@hgb/config/passport');

/**
 * Dababase connect
 */
mongoose.connect(DATABASE_URL, { useNewUrlParser: true }, err => {
  if (err) {
    throw err;
  }
  console.log(`  ${colors.green(DATABASE_URL)} connected`);
});

/**
 * Express app initialize
 */
const app = express();

/**
 * Middleware
 */
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(cookieParser());


/**
 * API endpoint
 */
app.use('/api', api);

/**
 * Services
 */
app.use('/services', services);

/**
 * 404 handler
 */
app.use((req, res, next) => {
  res.status(404).send({ message: '404'});
});

/**
 * Error handler
 */
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(500).send(
    err.stack
  );
});

export default app;