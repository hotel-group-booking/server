import { Schema, Document, model } from 'mongoose';

interface IRequestModel extends Document {
  user: string
  created: Date
  destination: string
  checkin: Date
  checkout: Date
  starRating: number
  groupType: string
  rooms: number
  budgetFrom: number
  budgetTo: number
  currency: string
  roomType: number
  roomTypeDetail: string
  additionInfo: string
  deleted: boolean
}

const schema = new Schema({
  user: { type: String, required: true },
  created: { type: Date, default: Date.now() },
  destination: { type: String, required: true },
  checkin: { type: Date, required: true },
  checkout: { type: Date, required: true },
  starRating: { type: Number, required: true },
  groupType: String,
  rooms: { type: Number, required: true },
  budgetFrom: { type: Number, required: true },
  budgetTo: { type: Number, required: true },
  roomType: { type: Number, required: true },
  roomTypeDetail: String,
  additionInfo: String,
  deleted: { type: Boolean, default: false }
});

const request = model<IRequestModel>('Request', schema);

export {
  IRequestModel,
  request as Request
}