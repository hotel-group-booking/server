import { Router } from 'express';
import { Request, Offer, IRequestModel } from '@hgb/models';
import { PER_PAGE } from '@hgb/config/environment';
import { filter, omit, slice } from 'ramda';
import { calculateWith, budget, roomType, starRating } from '@hgb/lib';

const router = Router();

/**
 * Checking...
 */

router.get('/check-status', async (req, res) => {
  return res.status(200).send({ message: 'OK' });
});

/**
 * GET
 */
router.get('/read-request', async (req, res, next) => {
  const { query: { id } } = req;
  try {
    const request = await Request.findById(id);
    if (!request) {
      return res.status(404).send({ message: `can not find request with id: ${id}`, request: null });
    }
    return res.status(200).send({ request, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.get('/get-request-by-user', async (req, res, next) => {
  const { query: { userId, page } } = req;
  try {
    const requests = await Request.find({ user: userId, deleted: false }, null, { skip: page * PER_PAGE });
    const total = await Request.countDocuments({ user: userId, deleted: false });
    return res.status(200).send({ requests, total, count: requests.length, message: 'OK' });
  } catch (err) {
    next(err)
  }
});

router.get('/read-offer', async (req, res, next) => {
  const { query: { id } } = req;
  try {
    const offer = await Offer.findById(id);
    if (!offer) {
      return res.status(404).send({ message: `can not find offer with id: ${id}`, offer: null });
    }
    return res.status(200).send({ offer, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.get('/get-offer-by-user', async (req, res, next) => {
  const { query: { userId, page } } = req;
  try {
    const offers = await Offer.find({ user: userId, deleted: false }, null, { skip: page * PER_PAGE });
    const total = await Offer.countDocuments({ user: userId, deletd: false });
    return res.status(200).send({ offers, total, count: offers.length, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.get('/get-offer-by-partner', async (req, res, next) => {
  const { query: { partnerId, page } } = req;
  try {
    const offers = await Offer.find({ partner: partnerId, deleted: false }, null, { skip: page * PER_PAGE });
    const total = await Offer.countDocuments({ partner: partnerId, deleted: false });
    return res.status(200).send({ offers, total, count: offers.length, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.get('/count', async (req, res, next) => {
  const { query: { included = true } } = req;
  const query = {};
  if (!JSON.parse(included)) {
    query['deleted'] = false;
  }
  try {
    const request = await Request.countDocuments(query);
    const offer = await Offer.countDocuments(query);
    return res.status(200).send({
      request,
      offer,
      message: 'OK'
    });
  } catch (err) {
    next(err);
  }
});

/**
 * POST
 */
router.post('/create-request', async (req, res, next) => {
  const { body } = req;
  try {
    const request = new Request(body);
    const result = await request.save();
    return res.status(200).send({ request: result, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/update-request', async (req, res, next) => {
  const { body: { id, ...rest }} = req;
  try {
    const updatedRequest = await Request.findByIdAndUpdate(id, rest, { new: true });
    if (!updatedRequest) {
      return res.status(404).send({ message: `can not update request with id: ${id}, may be not exsited`, request: null });
    }
    return res.status(200).send({ request: updatedRequest, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/delete-request', async (req, res, next) => {
  const { body: { id } } = req;
  try {
    const request = await Request.findByIdAndUpdate(id, { deleted: true }, { new: true });
    if (!request) {
      return res.status(404).send({ request: null, message: `can not delete request with id: ${id}, may be not exsited` });
    }
    return res.status(200).send({ request, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/destroy-request', async (req, res, next) => {
  const { body: { id } } = req;
  try {
    const request = await Request.findByIdAndDelete(id);
    if (!request) {
      return res.status(404).send({ request: null, message: `can not destroy request with id: ${id}, may be not exsited` });
    }
    return res.status(200).send({ request, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/create-offer', async (req, res, next) => {
  const { body } = req;
  try {
    const offer = new Offer(body);
    const result = await offer.save();
    return res.status(200).send({ offer: result, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/update-offer', async (req, res, next) => {
  const { body: { id, ...rest } } = req;
  try {
    const offer = await Offer.findByIdAndUpdate(id, rest, { new: true });
    if (!offer) {
      return res.status(404).send({ offer: null , message: `can not update offer with id: ${id}, may be not exsited` });
    }
    return res.status(200).send({ offer, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/delete-offer', async (req, res, next) => {
  const { body: { id } } = req;
  try {
    const offer = await Offer.findByIdAndUpdate(id, { deleted: true }, { new: true });
    if (!offer) {
      return res.status(404).send({ offer: null, message: `can not delete offer with id ${id}, may be not exsited` });
    }
    return res.status(200).send({ offer, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/destroy-offer', async (req, res, next) => {
  const { body: { id } } = req;
  try {
    const offer = await Offer.findByIdAndDelete(id);
    if (!offer) {
      return res.status(404).send({ offer: null, message: `can not destroy offer with id: ${id}, may be not exsited` });
    }
    return res.status(200).send({ offer, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

router.post('/filter-request', async (req, res, next) => {
  const { body, query: { page = 0 } } = req;
  try {
    const requests = await Request.find({ 
      deleted: false, 
      destination: { $in: body.destinations },
      rooms: { $lte: body.rooms }
    });

    const requestFilter = (criteria: any, reqs: IRequestModel[]) => {
      return filter(request => {
        const calculator = calculateWith(criteria, request);

        const budgetPoint = calculator(budget);
        const roomTypePoint = calculator(roomType);
        const starRatingPoint = calculator(starRating);
        return (budgetPoint + roomTypePoint + starRatingPoint) / 175 >= 0.7;
      }, reqs);
    }

    const filteredRequests = requestFilter(omit(['destinations', 'rooms'], body), requests);

    const result = slice(page * PER_PAGE, (1 + page) * PER_PAGE, filteredRequests);

    return res.status(200).send({ requests: result, total: filteredRequests.length, count:result.length, message: 'OK' });
  } catch (err) {
    next(err);
  }
});

export default router;