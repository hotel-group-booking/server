import { Server } from 'http';
import socketIO, { Namespace } from 'socket.io';

let notification: Namespace;

const socket = (server: Server) => {
  const io = socketIO(server);

  notification = io.of('/notification');

  notification.on('connect', socket => {
    console.log('Connected');
  })

  return io;
}

export {
  socket, notification
}